/*
  Aluno: Miguel Reis de Araújo
  NºUSP: 12752457
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PARAR '$'


int main()
{
	char palavra_tabu[20];

	scanf("%s\n", palavra_tabu);

	int tamanho_palavra_tabu = strlen(palavra_tabu);
	char *frase = NULL;
	int cont_letras = 1;
	int cont_palavra_tabu = 0;
	int indice_inicio_palavra = 0;
	int stop = 0;

	do {
		cont_letras++;

		frase = realloc(frase, cont_letras * sizeof(char));

		scanf("%c", &frase[cont_letras - 2]);

		if (frase[cont_letras - 2] == PARAR) {
			stop = 1;
		}

		frase[cont_letras - 1] = '\0';

		if ((cont_letras - 1) >= tamanho_palavra_tabu) {

			if (strcmp(palavra_tabu, &frase[indice_inicio_palavra]) == 0) {
				cont_palavra_tabu++;

				frase[indice_inicio_palavra] = '\0';

				cont_letras = indice_inicio_palavra + 1;
			}
		}

		if (((cont_letras - 1) - indice_inicio_palavra) >= tamanho_palavra_tabu) {
			indice_inicio_palavra++;
		}

	} while (!stop);

	frase[cont_letras - 2] = '\0';

	printf("A palavra tabu foi encontrada %d vezes\n", cont_palavra_tabu);
	printf("Frase: %s", frase);

	free(frase);

	return 0;
}
