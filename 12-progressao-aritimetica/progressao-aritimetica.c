#include <stdio.h>

int main()
{
    int long termo1, razao, tot_termos;
    scanf("%ld%ld%ld", &termo1, &razao, &tot_termos);

    int long termo_n = termo1;
    int long soma_termos = termo1;
    //tot_termos é diminuido em 1 porque o primeiro termo é definido fora do loop
    for (int i = 0; i < tot_termos - 1; i++) {
	termo_n += razao;
	soma_termos += termo_n;
    }

    printf("%ld\n%ld\n", termo_n, soma_termos);
    
    return 0;
}
