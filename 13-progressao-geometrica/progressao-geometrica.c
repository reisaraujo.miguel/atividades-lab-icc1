#include <stdio.h>

int main()
{
    float termo1, quociente;
    int long tot_termos;
    scanf("%f%f%ld", &termo1, &quociente, &tot_termos);

    float termo_n = termo1;
    float soma_termos = termo1;
    //tot_termos é diminuido em 1 porque o primeiro termo é definido fora do loop
    for (int i = 0; i < tot_termos - 1; i++) {
	termo_n *= quociente;
	soma_termos += termo_n;
    }

    printf("%.2f\n%.2f\n", termo_n, soma_termos);
    
    return 0;
}
