#include <stdio.h>

int main()
{
    char comandos[6];
    scanf(" %s", &*comandos);

    int coordenadas_y = 0;
    int coordenadas_x = 0;
    for (int i = 0; i < 6; i++) {
	switch (comandos[i]) {
	case 'W': coordenadas_y += 1;
	    break;
	case 'S': coordenadas_y -= 1;
	    break;
	case 'A': coordenadas_x -= 1;
	    break;
	case 'D': coordenadas_x += 1;
	    
	}
    }

    printf("%d %d\n", coordenadas_x, coordenadas_y);
    
    return 0;
}
