#include <stdio.h>

int main()
{
	int long coordenada_x1, coordenada_y1, largura1, altura1;
	scanf("%ld%ld%ld%ld", &coordenada_x1, &coordenada_y1, &largura1, &altura1);

	int long coordenada_x2, coordenada_y2, largura2, altura2;
	scanf("%ld%ld%ld%ld", &coordenada_x2, &coordenada_y2, &largura2, &altura2);

	int long coordenada_x3, coordenada_y3, largura3, altura3;

	// se o retangulo 1 está à direita do 2 e a distancia entre os dois não é maior que a largura do
	// retangulo 2
	if (coordenada_x1 >= coordenada_x2 && largura2 >= coordenada_x1 - coordenada_x2) {
		// calculo da largura3 se o retangulo 1 estiver a direita
		if (largura1 == 0) {
			largura3 = 0;
		}
		else if (largura1 > largura2 - (coordenada_x1 - coordenada_x2)) {
			largura3 = largura2 - (coordenada_x1 - coordenada_x2);
		}
		else {
			largura3 = largura1;
		}

		// se o retangulo 1 está acima do 2 e a distancia entre os ddois não é maior que a altura do
		// retangulo 1
		if (altura1 >= coordenada_y2 - coordenada_y1 && coordenada_y1 <= coordenada_y2) {
			coordenada_x3 = coordenada_x1; // o valor em x onde o eixo y1 intersecta com x2 é em x1
			coordenada_y3 = coordenada_y2; // o valor em y onde o eixo x2 intersecta com y1 é em y2
			if (altura2 == 0) {
				altura3 = 0;
			}
			else if (altura2 > altura1 - (coordenada_y2 - coordenada_y1)) {
				altura3 = altura1 - (coordenada_y2 - coordenada_y1);
			}
			else {
				altura3 = altura2;
			}
		}

		// se o retangulo 1 está abaixo do 2 e a distancia entre os dois não é maior que a altura do
		// retangulo 2
		else if (altura2 >= coordenada_y1 - coordenada_y2 && coordenada_y2 <= coordenada_y1) {
			coordenada_x3 = coordenada_x1;
			coordenada_y3 = coordenada_y1;
			if (altura1 == 0) {
				altura3 = 0;
			}
			else if (altura1 > altura2 - (coordenada_y1 - coordenada_y2)) {
				altura3 = altura2 - (coordenada_y1 - coordenada_y2);
			}
			else {
				altura3 = altura1;
			}
		}

		else {
			printf("MISS\n");
			return 0;
		}
	}

	else if (coordenada_x1 <= coordenada_x2 && largura1 >= coordenada_x2 - coordenada_x1) {
		if (largura2 == 0) {
			largura3 = 0;
		}
		else if (largura2 > largura1 - (coordenada_x2 - coordenada_x1)) {
			largura3 = largura1 - (coordenada_x2 - coordenada_x1);
		}
		else {
			largura3 = largura2;
		}

		if (altura2 >= coordenada_y1 - coordenada_y2 && coordenada_y2 <= coordenada_y1) {
			coordenada_x3 = coordenada_x2;
			coordenada_y3 = coordenada_y1;
			if (altura1 == 0) {
				altura3 = 0;
			}
			else if (altura1 > altura2 - (coordenada_y1 - coordenada_y2)) {
				altura3 = altura2 - (coordenada_y1 - coordenada_y2);
			}
			else {
				altura3 = altura1;
			}
		}

		else if (altura1 >= coordenada_y2 - coordenada_y1 && coordenada_y1 <= coordenada_y2) {
			coordenada_x3 = coordenada_x2;
			coordenada_y3 = coordenada_y2;
			if (altura2 == 0) {
				altura3 = 0;
			}
			else if (altura2 > altura1 - (coordenada_y2 - coordenada_y1)) {
				altura3 = altura1 - (coordenada_y2 - coordenada_y1);
			}
			else {
				altura3 = altura2;
			}
		}

		else {
			printf("MISS\n");
			return 0;
		}
	}

	else {
		printf("MISS\n");
		return 0;
	}

	printf("HIT: %ld %ld %ld %ld\n", coordenada_x3, coordenada_y3, largura3, altura3);

	return 0;
}
