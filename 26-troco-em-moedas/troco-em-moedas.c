#include <stdio.h>

void calculo_troco(int *valores_moedas);

#define MOEDA_1 0
#define MOEDA_5 1
#define MOEDA_10 2
#define MOEDA_25 3
#define MOEDA_50 4
#define REAL 5
	
int main()
{
    int valores_moedas[6] = {};
	scanf(" %d", &valores_moedas[MOEDA_1]);

	calculo_troco(valores_moedas);

	printf("O valor consiste em %d moedas de 1 real\n", valores_moedas[REAL]);
	printf("O valor consiste em %d moedas de 50 centavos\n", valores_moedas[MOEDA_50]);
	printf("O valor consiste em %d moedas de 25 centavos\n", valores_moedas[MOEDA_25]);
	printf("O valor consiste em %d moedas de 10 centavos\n", valores_moedas[MOEDA_10]);
	printf("O valor consiste em %d moedas de 5 centavos\n", valores_moedas[MOEDA_5]);
	printf("O valor consiste em %d moedas de 1 centavo\n", valores_moedas[MOEDA_1]);
	
    return 0;
}


void calculo_troco (int *valores_moedas)
{
	while (1) {
		if (valores_moedas[MOEDA_1] >= 100) {
			valores_moedas[REAL] += 1;
			valores_moedas[MOEDA_1] -= 100;
		}
		else if (valores_moedas[MOEDA_1] >= 50) {
			valores_moedas[MOEDA_50] += 1;
			valores_moedas[MOEDA_1] -= 50;
		}
		else if (valores_moedas[MOEDA_1] >= 25) {
			valores_moedas[MOEDA_25] += 1;
			valores_moedas[MOEDA_1] -= 25;
		}
		else if (valores_moedas[MOEDA_1] >= 10) {
			valores_moedas[MOEDA_10] += 1;
			valores_moedas[MOEDA_1] -= 10;
		}
		else if (valores_moedas[MOEDA_1] >= 5) {
			valores_moedas[MOEDA_5] += 1;
			valores_moedas[MOEDA_1] -= 5;
			break;
		}
		else {
			break;
		}
   	}
}
