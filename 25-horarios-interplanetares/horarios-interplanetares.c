#include <stdio.h>
#include <string.h>

void calc_tempo(unsigned long long *segundos, int *dia, int *hora, int *minuto, char *planeta);
long calc_tam_dia(char *planeta, int dia_terrestre, int duracao_hora, int duracao_minuto);


int main()
{
	unsigned long long segundo;
	scanf(" %llu", &segundo);

	char planeta[9];
	scanf("%s", planeta);
	scanf(" %*[^\n]");

	printf("%llu segundos no planeta %s equivalem a:\n", segundo, planeta);
	
	int minuto = 0;
	int hora = 0;
	int dia = 0;

	calc_tempo(&segundo, &dia, &hora, &minuto, planeta);
	
	printf("%d dias, %d horas, %d minutos e %llu segundos\n", dia, hora, minuto, segundo);

	return 0;
}


void calc_tempo(unsigned long long *segundos, int *dia, int *hora, int *minuto, char *planeta)
{
	int duracao_minuto = 60;
	int duracao_hora = duracao_minuto * 60;
	int dia_terrestre = duracao_hora * 24;
	long tamanho_dia;

	tamanho_dia = calc_tam_dia(planeta, dia_terrestre, duracao_hora, duracao_minuto);


	*dia = (*segundos) / tamanho_dia;
	*segundos -= (*dia) * tamanho_dia;

	*hora = (*segundos) / duracao_hora;
	*segundos -= (*hora) * duracao_hora;

	*minuto = (*segundos) / duracao_minuto;
	*segundos -= (*minuto) * duracao_minuto;
}


long calc_tam_dia(char *planeta, int dia_terrestre, int duracao_hora, int duracao_minuto)
{
	long calculo;

	if (strcmp(planeta, "Terra") == 0) {
		calculo = dia_terrestre;
	}
	else if (strcmp(planeta, "Venus") == 0) {
		calculo = dia_terrestre * 243;
	}
	else if (strcmp(planeta, "Mercurio") == 0) {
		calculo = (dia_terrestre * 58) + (duracao_hora * 16);
	}
	else if (strcmp(planeta, "Jupiter") == 0) {
		calculo = (duracao_hora * 9) + (duracao_minuto * 56);
	}

	return calculo;
}
