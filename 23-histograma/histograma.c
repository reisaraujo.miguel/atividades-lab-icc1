#include <stdio.h>

#define QT_PIXELS 25
#define QT_CORES 5

int main()
{
	int vetor_imagem[QT_PIXELS];
	for (int i = 0; i < QT_PIXELS; i++) {
		scanf("%d", &vetor_imagem[i]);
	}

	int valor_mais_presente;
	int contador_maior_incidencia = 0;

	for (int i = 0; i < QT_CORES; i++) {
		int contador_nova_incidencia = 0;

		printf("%d: |", i);

		for (int j = 0; j < QT_PIXELS; j++) {
			if (vetor_imagem[j] == i) {
				printf("#");
				contador_nova_incidencia += 1;
			}
		}

		if (contador_nova_incidencia > contador_maior_incidencia) {
			valor_mais_presente = i;
			contador_maior_incidencia = contador_nova_incidencia;
		}

		printf("\n");
	}

	for (int i = 0; i < QT_PIXELS; i++) {
		if (vetor_imagem[i] == valor_mais_presente) {
			printf("%d\n", i);
		}
	}

	return 0;
}
