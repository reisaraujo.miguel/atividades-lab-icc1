/*
  Aluno: Miguel Reis de Araújo
  NºUSP: 12752457
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PARAR '$'

char **read_matrix(int *indice_linha);
void substituir_sobrenome(char **matriz, char **matriz_nova, int comeco_sobrenome, int ln);


int main()
{
	int qt_nomes = 0;
	char **lista_nomes = read_matrix(&qt_nomes);

	qt_nomes++;

	char **lista_nova = malloc(qt_nomes * sizeof(char *));

	for (int i = 0; i < qt_nomes; i++) {
		lista_nova[i] = malloc((strlen(lista_nomes[i]) + 1) * sizeof(char));
		strcpy(lista_nova[i], lista_nomes[i]);
	}

	for (int i = 0; i < (qt_nomes - 1); i++) {
		if (i != 0 && (i % 2) != 0) {
			continue;
		}

		int contador_indices = 0;
		int comeco_sobrenome = 0;

		do {
			contador_indices++;

			if (lista_nomes[i][contador_indices] == ' ') {
				comeco_sobrenome = contador_indices + 1;
			}

		} while (lista_nomes[i][contador_indices] != '\0');

		substituir_sobrenome(lista_nomes, lista_nova, comeco_sobrenome, i);
	}

	for (int i = 0; i < qt_nomes; i++) {
		printf("%s\n", lista_nova[i]);
	}

	// limpando alocações
	for (int i = 0; i < qt_nomes; i++) {
		free(lista_nomes[i]);
	}

	free(lista_nomes);

	for (int i = 0; i < qt_nomes; i++) {
		free(lista_nova[i]);
	}

	free(lista_nova);

	return 0;
}


void substituir_sobrenome(char **matriz, char **matriz_nova, int comeco_sobrenome, int ln)
{
	char sobrenome[strlen(&matriz[ln][comeco_sobrenome])];

	strcpy(sobrenome, &matriz[ln][comeco_sobrenome]);
	ln++;

	int tamanho_nome = strlen(matriz[ln]);

	int novo_tamanho_nome = tamanho_nome + strlen(sobrenome) + 2;
	char string_temp[novo_tamanho_nome];

	strcpy(string_temp, matriz[ln]);
	string_temp[tamanho_nome] = ' ';
	string_temp[tamanho_nome + 1] = '\0';

	matriz_nova[ln] = realloc(matriz_nova[ln], novo_tamanho_nome * sizeof(char));

	strcpy(matriz_nova[ln], strcat(string_temp, sobrenome));
}


char **read_matrix(int *indice_linha)
{
	int contador_caracteres;
	int indice_coluna = 0;
	char **matriz = NULL;

	while (1) {
		matriz = realloc(matriz, (*indice_linha + 1) * sizeof(char *));

		matriz[(*indice_linha)] = NULL;

		contador_caracteres = 0;

		do {
			indice_coluna = contador_caracteres;

			contador_caracteres++;

			matriz[(*indice_linha)] =
				realloc(matriz[(*indice_linha)], contador_caracteres * sizeof(char));

			scanf("%c", &matriz[(*indice_linha)][indice_coluna]);

		} while (matriz[(*indice_linha)][indice_coluna] != '\n' &&
				 matriz[(*indice_linha)][indice_coluna] != '\r' &&
				 matriz[(*indice_linha)][indice_coluna] != PARAR);

		if (matriz[(*indice_linha)][indice_coluna] == PARAR)
			break;

		matriz[(*indice_linha)][indice_coluna] = '\0';

		(*indice_linha)++;
	}

	matriz[(*indice_linha)][indice_coluna] = '\0';

	scanf(" %*[\n]");
	return matriz;
}
