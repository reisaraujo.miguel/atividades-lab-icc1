#include <stdio.h>

int main()
{
    int vezes_brincou, vezes_comeu;
    scanf("%d%d", &vezes_brincou, &vezes_comeu);

    int diferenca_de_peso = 5*vezes_brincou - 3*vezes_comeu;

    if (diferenca_de_peso >= 30){
	printf("P\n");
    } else if (diferenca_de_peso >= 0) {
	printf("B\n");
    } else {
	printf("R\n");
    }
    return 0;
}
