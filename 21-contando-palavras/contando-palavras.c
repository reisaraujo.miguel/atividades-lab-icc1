#include <stdio.h>

#define TRUE 1
#define FALSE 0

int main()
{
	char caracteres;

	int stop = FALSE;
	int palavra_foi_iniciada = FALSE;
	int possivel_nova_linha = FALSE;

	int cont_caracteres = 0;
	int cont_palavras = 0;
	int cont_linhas = 0;

	do {
		stop = scanf("%c", &caracteres) == EOF;

		if (stop == TRUE) {
			if (palavra_foi_iniciada) {
				cont_palavras += 1;
			}
			continue;
		}

		cont_caracteres += 1;

		if (caracteres != ' ' && caracteres != '\t' && caracteres != '\n' && caracteres != '\r') {
			palavra_foi_iniciada = TRUE;
		}
		else if (palavra_foi_iniciada) {

			cont_palavras += 1;
			palavra_foi_iniciada = FALSE;
		}

		if (caracteres == '\r' && !possivel_nova_linha) {
			/* aqui eu verifico se o "possivel_nova_linha" é falso para caso você tenha mais de uma
			quebra de linha seguidas com '\r', nesse caso ele vai ler o primeiro e marcar que isso
			possivelmente indica uma nova linha. Então se for lido mais um '\r' ele vai adicionar
			mais 2 no valor de linhas. Caso seja lido um terceiro, o processo se repete. Caso seja
			lido qualquer coisa além de '\r' depois do primeiro, vai ser aumentado em 1 o número de
			linhas.*/
			possivel_nova_linha = TRUE;
		}
		else if (caracteres == '\r') {
			cont_linhas += 2;
			possivel_nova_linha = FALSE;
		}
		else if (caracteres == '\n' || possivel_nova_linha) {
			cont_linhas += 1;
			possivel_nova_linha = FALSE;
		}

	} while (!stop);

	printf("Linhas\tPalav.\tCarac.\n");
	printf("%d\t%d\t%d\n", cont_linhas, cont_palavras, cont_caracteres);

	return 0;
}
