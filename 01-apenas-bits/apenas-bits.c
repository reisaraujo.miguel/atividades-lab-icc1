#include <stdio.h>

int main()
{
    int numero;
    
    scanf("%i", &numero);

    printf("%c%c%c%c\n", numero >> 24, numero >> 16, numero >> 8, numero);
    
    return 0;
}
