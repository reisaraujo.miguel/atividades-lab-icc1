#include <stdio.h>

int main()
{
    int base, expoente = 0;
    int resultado = 1;
    scanf("%d%d", &base, &expoente);

    for (int i = 0; i < expoente; i++){
	resultado *= base;
    }

    printf("%d\n", resultado);
    return 0;
}
