/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 */

#include <stdio.h>
#include <stdlib.h>

void adiciona_cabecalho(FILE *arquivo);
void adiciona_rodape(FILE *arquivo);


int main()
{
	FILE *arquivo = fopen("code.c", "w");
	char comando;
	int stop;

	adiciona_cabecalho(arquivo);

	do {
		stop = scanf("%c", &comando) == EOF;

		switch (comando) {
			case '+': fprintf(arquivo, "\tmem[i]++;\n"); break;
			case '-': fprintf(arquivo, "\tmem[i]--;\n"); break;
			case '>': fprintf(arquivo, "\ti++;\n"); break;
			case '<': fprintf(arquivo, "\ti--;\n"); break;
			case '.': fprintf(arquivo, "\tputchar(mem[i]);\n"); break;
			case '[': fprintf(arquivo, "\n\twhile (mem[i]) {\n"); break;
			case ']': fprintf(arquivo, "\t}\n\n");
		}
	} while (!stop);

	adiciona_rodape(arquivo);

	fclose(arquivo);

	system("gcc code.c -o code");
	system("./code");

	return 0;
}


void adiciona_cabecalho(FILE *arquivo)
{
	fprintf(arquivo, "#include <stdio.h>\n\n\n");
	fprintf(arquivo, "int main() \n{\n");
	fprintf(arquivo, "\tchar mem[30000];\n");
	fprintf(arquivo, "\tint i = 0;\n\n");
	fprintf(arquivo, "\tfor (int j = 0; j < 30000; j++) {\n");
	fprintf(arquivo, "\t\tmem[j] = 0;\n");
	fprintf(arquivo, "\t}\n\n");
}


void adiciona_rodape(FILE *arquivo)
{
	fprintf(arquivo, "\n\tprintf(\"\\n\");\n\n");
	fprintf(arquivo, "\tfor (int j = 0; j < 30000; j++) {\n");
	fprintf(arquivo, "\t\tif (mem[j] != 0) {\n");
	fprintf(arquivo, "\t\t\tprintf(\"%cd \", mem[j]);\n", '%');
	fprintf(arquivo, "\t\t}\n");
	fprintf(arquivo, "\t}\n");
	fprintf(arquivo, "\n\tprintf(\"\\n\");\n");
	fprintf(arquivo, "\n\treturn 0;\n");
	fprintf(arquivo, "}");
}
